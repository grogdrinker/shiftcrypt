ORIGIN        This file is based on BMRB entry bmr17926, PDB code 2lj9.
DATE          Generated on 2012-08-23, original BMRB deposition date 2011-09-09.
MOLNAME       CP12_PROTEIN
TEMPERATURE   293.0
pH            7.0
IONICSTRENGTH n/a
LABELLING     13C (uniform), 15N (uniform) (original '[U-100% 13C; U-100% 15N]')
LABELLING     13C (uniform), 15N (uniform) (original '[U-13C; U-15N]')
LABELLING     15N (uniform) (original '[U-15N]')
LABELLING     None (natural abundance) (original 'natural abundance')
EXPTYPE       NMR

    
CORRECTION    C (aliphatic)                   -0.084 +/- 0.132 (NOT APPLIED, TOO UNCERTAIN)
CORRECTION    C (high ppm, proton attached)   -0.084 +/- 0.132 (correction based on aliphatic) (NOT APPLIED, TOO UNCERTAIN)
CORRECTION    C (high ppm, no proton)         -0.043 +/- 0.309 (NOT APPLIED, TOO UNCERTAIN)
CORRECTION    N                               -0.379 +/- 0.629 (NOT APPLIED, TOO UNCERTAIN)
CORRECTION    H                               -0.012 +/- 0.016 (NOT APPLIED, TOO UNCERTAIN)

MODEL 1
ATOM      1 C    SER A  57     -11.721   1.599   3.489  C Coil         173.830         
ATOM      2 CA   SER A  57     -11.849   0.803   4.778  C Coil          58.500         
ATOM      3 CB   SER A  57     -10.479   0.708   5.455  C Coil          64.050         
ATOM      4 H    SER A  57     -11.848  -0.940   3.685  C Coil           8.230         
ATOM      5 HA   SER A  57     -12.539   1.305   5.440  C Coil           4.440         
ATOM      6 HB3  SER A  57      -9.788   1.381   4.960  C Coil           3.870         
ATOM      7 HG   SER A  57     -11.328   0.640   7.220  C Coil         
ATOM      8 N    SER A  57     -12.351  -0.572   4.517  C Coil         115.720         
ATOM      9 O    SER A  57     -12.046   1.111   2.406  C Coil         
ATOM     10 OG   SER A  57     -10.560   1.060   6.825  C Coil         
ATOM     11 C    ASP A  58     -10.011   3.064   1.515  C Coil         
ATOM     12 CA   ASP A  58     -11.024   3.689   2.468  C Coil          52.040         
ATOM     13 CB   ASP A  58     -10.546   5.071   2.918  C Coil          41.840         
ATOM     14 CG   ASP A  58     -11.407   6.190   2.363  C Coil         
ATOM     15 H    ASP A  58     -10.972   3.143   4.507  C Coil           8.600         
ATOM     16 HA   ASP A  58     -11.971   3.789   1.957  C Coil           4.970         
ATOM     17 HB3  ASP A  58      -9.531   5.223   2.583  C Coil           2.870   2.660 
ATOM     18 N    ASP A  58     -11.224   2.821   3.617  C Coil         123.580         
ATOM     19 O    ASP A  58      -9.234   2.194   1.910  C Coil         
ATOM     20 OD1  ASP A  58     -12.609   5.950   2.123  C Coil         
ATOM     21 OD2  ASP A  58     -10.879   7.305   2.170  C Coil         
ATOM     22 C    PRO A  59      -7.625   2.919  -0.308  H AlphaHelix   178.090         
ATOM     23 CA   PRO A  59      -9.077   2.967  -0.769  H AlphaHelix    64.130         
ATOM     24 CB   PRO A  59      -9.228   3.956  -1.920  H AlphaHelix    32.210         
ATOM     25 CD   PRO A  59     -10.901   4.525  -0.315  H AlphaHelix   
ATOM     26 CG   PRO A  59     -10.618   4.471  -1.791  H AlphaHelix    27.520         
ATOM     27 HA   PRO A  59      -9.372   1.991  -1.090  H AlphaHelix     4.440         
ATOM     28 HB3  PRO A  59      -9.080   3.445  -2.853  H AlphaHelix     2.360   2.010 
ATOM     29 HD3  PRO A  59     -11.934   4.282  -0.119  H AlphaHelix   
ATOM     30 HG3  PRO A  59     -11.306   3.798  -2.280  H AlphaHelix   
ATOM     31 N    PRO A  59     -10.003   3.494   0.244  H AlphaHelix   
ATOM     32 O    PRO A  59      -6.772   2.324  -0.966  H AlphaHelix   
ATOM     33 C    LEU A  60      -5.549   2.201   1.800  H AlphaHelix   177.750         
ATOM     34 CA   LEU A  60      -6.009   3.592   1.374  H AlphaHelix    56.150         
ATOM     35 CB   LEU A  60      -5.962   4.546   2.570  H AlphaHelix    41.680         
ATOM     36 CD1  LEU A  60      -3.649   5.514   2.577  H AlphaHelix    24.830  23.930 
ATOM     37 CD2  LEU A  60      -4.821   5.066   4.741  H AlphaHelix    24.830  23.930 
ATOM     38 CG   LEU A  60      -4.621   4.600   3.306  H AlphaHelix    27.050         
ATOM     39 H    LEU A  60      -8.081   4.001   1.280  H AlphaHelix     8.430         
ATOM     40 HA   LEU A  60      -5.343   3.959   0.608  H AlphaHelix     4.340         
ATOM     41 HB3  LEU A  60      -6.721   4.243   3.275  H AlphaHelix     1.700         
ATOM     42 HD11 LEU A  60      -3.688   5.310   1.518  H AlphaHelix     0.850         
ATOM     43 HD12 LEU A  60      -2.647   5.337   2.941  H AlphaHelix     0.850         
ATOM     44 HD13 LEU A  60      -3.919   6.545   2.756  H AlphaHelix     0.850         
ATOM     45 HD21 LEU A  60      -4.607   6.122   4.809  H AlphaHelix     0.850         
ATOM     46 HD22 LEU A  60      -4.153   4.521   5.392  H AlphaHelix     0.850         
ATOM     47 HD23 LEU A  60      -5.843   4.884   5.040  H AlphaHelix     0.850         
ATOM     48 HG   LEU A  60      -4.192   3.609   3.332  H AlphaHelix   
ATOM     49 N    LEU A  60      -7.355   3.551   0.816  H AlphaHelix   120.900         
ATOM     50 O    LEU A  60      -4.404   1.817   1.563  H AlphaHelix   
ATOM     51 C    GLU A  61      -5.903  -0.838   1.729  H AlphaHelix   178.630         
ATOM     52 CA   GLU A  61      -6.126   0.108   2.899  H AlphaHelix    59.450         
ATOM     53 CB   GLU A  61      -7.242  -0.425   3.798  H AlphaHelix    29.660         
ATOM     54 CD   GLU A  61      -7.745  -1.897   5.788  H AlphaHelix   
ATOM     55 CG   GLU A  61      -6.850  -1.662   4.588  H AlphaHelix    36.420         
ATOM     56 H    GLU A  61      -7.341   1.816   2.599  H AlphaHelix     7.960         
ATOM     57 HA   GLU A  61      -5.216   0.167   3.468  H AlphaHelix     3.910         
ATOM     58 HB3  GLU A  61      -8.096  -0.671   3.185  H AlphaHelix     2.110         
ATOM     59 HG3  GLU A  61      -5.832  -1.546   4.933  H AlphaHelix   
ATOM     60 N    GLU A  61      -6.446   1.453   2.435  H AlphaHelix   120.390         
ATOM     61 O    GLU A  61      -4.865  -1.497   1.636  H AlphaHelix   
ATOM     62 OE1  GLU A  61      -8.203  -0.905   6.391  H AlphaHelix   
ATOM     63 OE2  GLU A  61      -7.990  -3.075   6.126  H AlphaHelix   
ATOM     64 C    GLU A  62      -5.555  -1.431  -1.152  H AlphaHelix   178.320         
ATOM     65 CA   GLU A  62      -6.785  -1.770  -0.330  H AlphaHelix    56.650         
ATOM     66 CB   GLU A  62      -8.025  -1.645  -1.208  H AlphaHelix    29.580         
ATOM     67 CD   GLU A  62     -10.388  -2.416  -1.653  H AlphaHelix   
ATOM     68 CG   GLU A  62      -9.236  -2.388  -0.669  H AlphaHelix    36.350         
ATOM     69 H    GLU A  62      -7.676  -0.351   0.963  H AlphaHelix     8.150         
ATOM     70 HA   GLU A  62      -6.701  -2.787   0.019  H AlphaHelix     4.120         
ATOM     71 HB3  GLU A  62      -7.789  -2.042  -2.187  H AlphaHelix     2.060         
ATOM     72 HG3  GLU A  62      -9.567  -1.901   0.237  H AlphaHelix   
ATOM     73 N    GLU A  62      -6.879  -0.902   0.836  H AlphaHelix   118.100         
ATOM     74 O    GLU A  62      -4.999  -2.292  -1.835  H AlphaHelix   
ATOM     75 OE1  GLU A  62     -10.136  -2.646  -2.855  H AlphaHelix   
ATOM     76 OE2  GLU A  62     -11.542  -2.207  -1.224  H AlphaHelix   
ATOM     77 C    TYR A  63      -2.735  -0.483  -1.351  H AlphaHelix   178.780         
ATOM     78 CA   TYR A  63      -3.961   0.244  -1.844  H AlphaHelix    61.510         
ATOM     79 CB   TYR A  63      -3.710   1.736  -1.702  H AlphaHelix    38.590         
ATOM     80 CD1  TYR A  63      -5.313   2.421  -3.525  H AlphaHelix   
ATOM     81 CD2  TYR A  63      -3.139   3.398  -3.498  H AlphaHelix   
ATOM     82 CE1  TYR A  63      -5.629   3.148  -4.655  H AlphaHelix   
ATOM     83 CE2  TYR A  63      -3.447   4.128  -4.625  H AlphaHelix   
ATOM     84 CG   TYR A  63      -4.064   2.534  -2.930  H AlphaHelix   
ATOM     85 CZ   TYR A  63      -4.693   4.002  -5.202  H AlphaHelix   
ATOM     86 H    TYR A  63      -5.606   0.474  -0.532  H AlphaHelix     8.080         
ATOM     87 HA   TYR A  63      -4.123   0.006  -2.884  H AlphaHelix     4.290         
ATOM     88 HB3  TYR A  63      -2.652   1.877  -1.510  H AlphaHelix     3.210   3.020 
ATOM     89 HD1  TYR A  63      -6.042   1.753  -3.093  H AlphaHelix     7.060         
ATOM     90 HD2  TYR A  63      -2.165   3.494  -3.045  H AlphaHelix     7.060         
ATOM     91 HE1  TYR A  63      -6.604   3.047  -5.104  H AlphaHelix     6.880         
ATOM     92 HE2  TYR A  63      -2.710   4.791  -5.049  H AlphaHelix     6.880         
ATOM     93 HH   TYR A  63      -4.241   4.759  -6.911  H AlphaHelix   
ATOM     94 N    TYR A  63      -5.129  -0.174  -1.091  H AlphaHelix   120.940         
ATOM     95 O    TYR A  63      -2.107  -1.234  -2.092  H AlphaHelix   
ATOM     96 OH   TYR A  63      -5.003   4.728  -6.329  H AlphaHelix   
ATOM     97 C    CYS A  64      -1.309  -2.430   0.235  H AlphaHelix   176.760         
ATOM     98 CA   CYS A  64      -1.228  -0.922   0.466  H AlphaHelix    55.230         
ATOM     99 CB   CYS A  64      -1.013  -0.562   1.941  H AlphaHelix    36.570         
ATOM    100 H    CYS A  64      -2.928   0.336   0.464  H AlphaHelix     8.810         
ATOM    101 HA   CYS A  64      -0.390  -0.550  -0.111  H AlphaHelix     4.540         
ATOM    102 HB3  CYS A  64      -0.513  -1.382   2.436  H AlphaHelix     3.070   2.810 
ATOM    103 N    CYS A  64      -2.392  -0.268  -0.092  H AlphaHelix   116.400         
ATOM    104 O    CYS A  64      -0.312  -3.136   0.312  H AlphaHelix   
ATOM    105 SG   CYS A  64       0.002   0.944   2.187  H AlphaHelix   
ATOM    106 C    LYS A  65      -2.325  -4.560  -1.855  H AlphaHelix   177.870         
ATOM    107 CA   LYS A  65      -2.665  -4.327  -0.376  H AlphaHelix    59.050         
ATOM    108 CB   LYS A  65      -4.101  -4.772  -0.077  H AlphaHelix    32.000         
ATOM    109 CD   LYS A  65      -5.895  -6.357  -0.849  H AlphaHelix   
ATOM    110 CE   LYS A  65      -6.330  -5.438  -1.981  H AlphaHelix   
ATOM    111 CG   LYS A  65      -4.417  -6.190  -0.527  H AlphaHelix    24.960         
ATOM    112 H    LYS A  65      -3.273  -2.323  -0.141  H AlphaHelix     7.700         
ATOM    113 HA   LYS A  65      -1.977  -4.888   0.240  H AlphaHelix     4.070         
ATOM    114 HB3  LYS A  65      -4.782  -4.099  -0.574  H AlphaHelix     1.900         
ATOM    115 HD3  LYS A  65      -6.473  -6.125   0.033  H AlphaHelix     1.620         
ATOM    116 HE3  LYS A  65      -5.492  -4.821  -2.271  H AlphaHelix   
ATOM    117 HG3  LYS A  65      -4.151  -6.876   0.265  H AlphaHelix     1.440         
ATOM    118 HZ1  LYS A  65      -7.268  -5.565  -3.844  H AlphaHelix   
ATOM    119 HZ2  LYS A  65      -7.480  -6.936  -2.875  H AlphaHelix   
ATOM    120 HZ3  LYS A  65      -5.997  -6.662  -3.641  H AlphaHelix   
ATOM    121 N    LYS A  65      -2.500  -2.920  -0.077  H AlphaHelix   121.420         
ATOM    122 NZ   LYS A  65      -6.801  -6.204  -3.168  H AlphaHelix   
ATOM    123 O    LYS A  65      -1.707  -5.563  -2.212  H AlphaHelix   
ATOM    124 C    ASP A  66      -1.385  -2.851  -4.698  H AlphaHelix   175.740         
ATOM    125 CA   ASP A  66      -2.544  -3.717  -4.160  H AlphaHelix    54.990         
ATOM    126 CB   ASP A  66      -3.825  -3.298  -4.871  H AlphaHelix    41.970         
ATOM    127 CG   ASP A  66      -4.967  -4.268  -4.636  H AlphaHelix   
ATOM    128 H    ASP A  66      -3.275  -2.862  -2.368  H AlphaHelix     7.180         
ATOM    129 HA   ASP A  66      -2.345  -4.749  -4.397  H AlphaHelix     4.680         
ATOM    130 HB3  ASP A  66      -3.634  -3.244  -5.927  H AlphaHelix     2.670   2.450 
ATOM    131 N    ASP A  66      -2.764  -3.626  -2.714  H AlphaHelix   116.020         
ATOM    132 O    ASP A  66      -1.219  -2.758  -5.912  H AlphaHelix   
ATOM    133 OD1  ASP A  66      -4.699  -5.482  -4.516  H AlphaHelix   
ATOM    134 OD2  ASP A  66      -6.128  -3.813  -4.573  H AlphaHelix   
ATOM    135 C    ASN A  67       1.834  -1.912  -4.344  T TurnI        
ATOM    136 CA   ASN A  67       0.444  -1.284  -4.292  T TurnI         51.600         
ATOM    137 CB   ASN A  67       0.439  -0.014  -3.459  T TurnI         39.850         
ATOM    138 CG   ASN A  67      -0.618   0.939  -3.933  T TurnI        178.170         
ATOM    139 H    ASN A  67      -0.825  -2.219  -2.883  T TurnI          7.510         
ATOM    140 HA   ASN A  67       0.220  -1.005  -5.276  T TurnI          5.120         
ATOM    141 HB3  ASN A  67       1.385   0.466  -3.535  T TurnI          2.400   2.030 
ATOM    142 HD21 ASN A  67      -1.986  -0.371  -3.386  T TurnI          6.520         
ATOM    143 HD22 ASN A  67      -2.573   1.077  -4.095  T TurnI          7.030         
ATOM    144 N    ASN A  67      -0.630  -2.171  -3.833  T TurnI        117.170         
ATOM    145 ND2  ASN A  67      -1.852   0.512  -3.789  T TurnI        118.090         
ATOM    146 O    ASN A  67       2.624  -1.786  -3.421  T TurnI        
ATOM    147 OD1  ASN A  67      -0.331   2.034  -4.415  T TurnI        
ATOM    148 C    PRO A  68       4.599  -2.257  -5.136  T TurnIV       177.710         
ATOM    149 CA   PRO A  68       3.488  -3.174  -5.650  T TurnIV        65.240         
ATOM    150 CB   PRO A  68       3.604  -3.371  -7.168  T TurnIV        32.230         
ATOM    151 CD   PRO A  68       1.317  -2.738  -6.629  T TurnIV        50.350         
ATOM    152 CG   PRO A  68       2.281  -2.983  -7.760  T TurnIV        27.500         
ATOM    153 HA   PRO A  68       3.554  -4.130  -5.151  T TurnIV         4.400         
ATOM    154 HB3  PRO A  68       3.834  -4.406  -7.375  T TurnIV         2.350   2.010 
ATOM    155 HD3  PRO A  68       0.662  -3.582  -6.489  T TurnIV         3.570   3.360 
ATOM    156 HG3  PRO A  68       1.920  -3.784  -8.387  T TurnIV         2.030   1.920 
ATOM    157 N    PRO A  68       2.174  -2.567  -5.463  T TurnIV       
ATOM    158 O    PRO A  68       5.676  -2.716  -4.765  T TurnIV       
ATOM    159 C    GLU A  69       4.786   0.566  -3.235  T TurnIV       177.110         
ATOM    160 CA   GLU A  69       5.245   0.044  -4.602  T TurnIV        56.130         
ATOM    161 CB   GLU A  69       5.368   1.202  -5.594  T TurnIV        28.850         
ATOM    162 CD   GLU A  69       3.774   1.941  -7.412  T TurnIV       
ATOM    163 CG   GLU A  69       4.042   1.872  -5.921  T TurnIV        36.480         
ATOM    164 H    GLU A  69       3.420  -0.663  -5.402  T TurnIV         8.740         
ATOM    165 HA   GLU A  69       6.209  -0.432  -4.490  T TurnIV         4.390         
ATOM    166 HB3  GLU A  69       5.795   0.830  -6.513  T TurnIV         2.220   1.970 
ATOM    167 HG3  GLU A  69       4.054   2.877  -5.525  T TurnIV         2.290         
ATOM    168 N    GLU A  69       4.304  -0.956  -5.100  T TurnIV       117.090         
ATOM    169 O    GLU A  69       5.602   0.900  -2.376  T TurnIV       
ATOM    170 OE1  GLU A  69       4.572   2.581  -8.129  T TurnIV       
ATOM    171 OE2  GLU A  69       2.768   1.356  -7.862  T TurnIV       
ATOM    172 C    THR A  70       2.181  -0.065  -1.051  T TurnIV       176.260         
ATOM    173 CA   THR A  70       2.861   1.081  -1.790  T TurnIV        61.400         
ATOM    174 CB   THR A  70       1.871   2.223  -2.041  T TurnIV        70.900         
ATOM    175 CG2  THR A  70       2.084   3.410  -1.125  T TurnIV        22.940         
ATOM    176 H    THR A  70       2.881   0.325  -3.773  T TurnIV         7.350         
ATOM    177 HA   THR A  70       3.661   1.440  -1.151  T TurnIV         4.530         
ATOM    178 HB   THR A  70       0.866   1.859  -1.881  T TurnIV         4.340         
ATOM    179 HG1  THR A  70       1.087   2.868  -3.716  T TurnIV       
ATOM    180 HG21 THR A  70       2.596   4.194  -1.664  T TurnIV         1.370         
ATOM    181 HG22 THR A  70       2.681   3.108  -0.277  T TurnIV         1.370         
ATOM    182 HG23 THR A  70       1.128   3.775  -0.781  T TurnIV         1.370         
ATOM    183 N    THR A  70       3.466   0.618  -3.048  T TurnIV       111.550         
ATOM    184 O    THR A  70       1.709   0.102   0.062  T TurnIV       
ATOM    185 OG1  THR A  70       1.967   2.688  -3.376  T TurnIV       
ATOM    186 C    ASN A  71       2.888  -2.922  -0.131  T TurnIV       177.720         
ATOM    187 CA   ASN A  71       1.711  -2.422  -0.938  T TurnIV        56.760         
ATOM    188 CB   ASN A  71       1.165  -3.507  -1.898  T TurnIV        38.330         
ATOM    189 CG   ASN A  71       1.984  -4.788  -1.910  T TurnIV       176.240         
ATOM    190 H    ASN A  71       2.674  -1.357  -2.479  T TurnIV         9.140         
ATOM    191 HA   ASN A  71       0.940  -2.094  -0.264  T TurnIV         4.380         
ATOM    192 HB3  ASN A  71       1.142  -3.117  -2.892  T TurnIV         2.860         
ATOM    193 HD21 ASN A  71       3.640  -3.752  -2.273  T TurnIV         7.820         
ATOM    194 HD22 ASN A  71       3.834  -5.466  -2.150  T TurnIV         7.050         
ATOM    195 N    ASN A  71       2.218  -1.252  -1.626  T TurnIV       122.700         
ATOM    196 ND2  ASN A  71       3.284  -4.656  -2.133  T TurnIV       113.350         
ATOM    197 O    ASN A  71       2.765  -3.696   0.818  T TurnIV       
ATOM    198 OD1  ASN A  71       1.450  -5.881  -1.720  T TurnIV       
ATOM    199 C    GLU A  72       5.462  -1.465   1.127  T TurnIV       176.810         
ATOM    200 CA   GLU A  72       5.301  -2.591   0.145  T TurnIV        58.660         
ATOM    201 CB   GLU A  72       6.481  -2.600  -0.833  T TurnIV        28.820         
ATOM    202 CD   GLU A  72       6.107  -4.594  -2.333  T TurnIV       
ATOM    203 CG   GLU A  72       6.130  -3.082  -2.225  T TurnIV        36.720         
ATOM    204 H    GLU A  72       4.000  -1.673  -1.240  T TurnIV         9.800         
ATOM    205 HA   GLU A  72       5.250  -3.531   0.676  T TurnIV         4.230         
ATOM    206 HB3  GLU A  72       7.253  -3.244  -0.439  T TurnIV         2.120         
ATOM    207 HG3  GLU A  72       6.865  -2.700  -2.917  T TurnIV       
ATOM    208 N    GLU A  72       4.034  -2.360  -0.514  T TurnIV       118.080         
ATOM    209 O    GLU A  72       6.478  -1.335   1.810  T TurnIV       
ATOM    210 OE1  GLU A  72       6.657  -5.263  -1.432  T TurnIV       
ATOM    211 OE2  GLU A  72       5.539  -5.111  -3.318  T TurnIV       
ATOM    212 C    CYS A  73       4.844   0.269   3.345  T TurnIV       175.280         
ATOM    213 CA   CYS A  73       4.461   0.594   1.912  T TurnIV        55.370         
ATOM    214 CB   CYS A  73       3.069   1.239   1.927  T TurnIV        39.610         
ATOM    215 H    CYS A  73       3.712  -0.739   0.494  T TurnIV         7.690         
ATOM    216 HA   CYS A  73       5.162   1.251   1.453  T TurnIV         4.840         
ATOM    217 HB3  CYS A  73       2.730   1.379   0.915  T TurnIV         3.170   2.880 
ATOM    218 N    CYS A  73       4.452  -0.596   1.113  T TurnIV       117.020         
ATOM    219 O    CYS A  73       5.454   1.076   4.048  T TurnIV       
ATOM    220 SG   CYS A  73       1.807   0.247   2.823  T TurnIV       
ATOM    221 C    ARG A  74       6.256  -1.638   5.287  T TurnIV       176.500         
ATOM    222 CA   ARG A  74       4.760  -1.405   5.103  T TurnIV        57.120         
ATOM    223 CB   ARG A  74       3.989  -2.697   5.386  T TurnIV        30.760         
ATOM    224 CD   ARG A  74       3.005  -3.726   7.458  T TurnIV       
ATOM    225 CG   ARG A  74       4.280  -3.293   6.754  T TurnIV        27.030         
ATOM    226 CZ   ARG A  74       3.616  -3.709   9.843  T TurnIV       
ATOM    227 H    ARG A  74       3.989  -1.508   3.127  T TurnIV         7.790         
ATOM    228 HA   ARG A  74       4.438  -0.645   5.799  T TurnIV         4.400         
ATOM    229 HB3  ARG A  74       4.248  -3.428   4.635  T TurnIV         1.840         
ATOM    230 HD3  ARG A  74       2.475  -4.420   6.821  T TurnIV         3.230         
ATOM    231 HE   ARG A  74       3.215  -5.348   8.777  T TurnIV         7.190         
ATOM    232 HG3  ARG A  74       4.781  -2.551   7.359  T TurnIV         1.680         
ATOM    233 HH11 ARG A  74       3.529  -1.878   8.987  T TurnIV       
ATOM    234 HH12 ARG A  74       3.964  -1.894  10.663  T TurnIV       
ATOM    235 HH21 ARG A  74       3.784  -5.371  10.980  T TurnIV       
ATOM    236 HH22 ARG A  74       4.109  -3.878  11.795  T TurnIV       
ATOM    237 N    ARG A  74       4.473  -0.928   3.756  T TurnIV       121.930         
ATOM    238 NE   ARG A  74       3.281  -4.370   8.738  T TurnIV        84.520         
ATOM    239 NH1  ARG A  74       3.711  -2.385   9.830  T TurnIV       
ATOM    240 NH2  ARG A  74       3.856  -4.374  10.965  T TurnIV       
ATOM    241 O    ARG A  74       6.802  -1.412   6.367  T TurnIV       
ATOM    242 C    THR A  75       9.138  -1.151   3.765  T TurnIV       174.100         
ATOM    243 CA   THR A  75       8.346  -2.356   4.265  T TurnIV        61.720         
ATOM    244 CB   THR A  75       8.680  -3.587   3.422  T TurnIV        69.700         
ATOM    245 CG2  THR A  75       9.824  -4.403   3.984  T TurnIV        21.480         
ATOM    246 H    THR A  75       6.422  -2.251   3.393  T TurnIV         8.100         
ATOM    247 HA   THR A  75       8.618  -2.549   5.292  T TurnIV         4.340         
ATOM    248 HB   THR A  75       8.960  -3.266   2.429  T TurnIV         4.200         
ATOM    249 HG1  THR A  75       7.146  -4.542   4.177  T TurnIV       
ATOM    250 HG21 THR A  75      10.266  -4.994   3.195  T TurnIV         1.150         
ATOM    251 HG22 THR A  75       9.452  -5.058   4.757  T TurnIV         1.150         
ATOM    252 HG23 THR A  75      10.569  -3.742   4.399  T TurnIV         1.150         
ATOM    253 N    THR A  75       6.913  -2.091   4.225  T TurnIV       113.200         
ATOM    254 O    THR A  75       9.904  -0.546   4.514  T TurnIV       
ATOM    255 OG1  THR A  75       7.555  -4.440   3.314  T TurnIV       
ATOM    256 C    TYR A  76       8.821   1.596   2.011  T TurnIV       175.280         
ATOM    257 CA   TYR A  76       9.647   0.321   1.894  T TurnIV        57.920         
ATOM    258 CB   TYR A  76       9.957   0.034   0.422  T TurnIV        39.080         
ATOM    259 CD1  TYR A  76      12.178   1.194   0.106  T TurnIV       
ATOM    260 CD2  TYR A  76      10.327   1.931  -1.202  T TurnIV       
ATOM    261 CE1  TYR A  76      12.985   2.142  -0.494  T TurnIV       
ATOM    262 CE2  TYR A  76      11.127   2.882  -1.808  T TurnIV       
ATOM    263 CG   TYR A  76      10.837   1.073  -0.236  T TurnIV       
ATOM    264 CZ   TYR A  76      12.455   2.983  -1.449  T TurnIV       
ATOM    265 H    TYR A  76       8.326  -1.333   1.947  T TurnIV         8.060         
ATOM    266 HA   TYR A  76      10.575   0.458   2.427  T TurnIV         4.590         
ATOM    267 HB3  TYR A  76       9.031  -0.016  -0.130  T TurnIV         3.160   2.950 
ATOM    268 HD1  TYR A  76      12.590   0.534   0.855  T TurnIV         7.140         
ATOM    269 HD2  TYR A  76       9.286   1.849  -1.481  T TurnIV         7.140         
ATOM    270 HE1  TYR A  76      14.025   2.221  -0.213  T TurnIV         6.810         
ATOM    271 HE2  TYR A  76      10.712   3.540  -2.556  T TurnIV         6.810         
ATOM    272 HH   TYR A  76      13.320   3.744  -2.989  T TurnIV       
ATOM    273 N    TYR A  76       8.949  -0.810   2.494  T TurnIV       121.920         
ATOM    274 O    TYR A  76       7.642   1.556   2.364  T TurnIV       
ATOM    275 OH   TYR A  76      13.257   3.929  -2.049  T TurnIV       
ATOM    276 C    ASP A  77       8.417   4.521   0.390  C Coil         174.820         
ATOM    277 CA   ASP A  77       8.780   4.018   1.784  C Coil          54.300         
ATOM    278 CB   ASP A  77       9.673   5.040   2.496  C Coil          41.200         
ATOM    279 CG   ASP A  77       9.001   5.644   3.712  C Coil         
ATOM    280 H    ASP A  77      10.390   2.686   1.441  C Coil           8.310         
ATOM    281 HA   ASP A  77       7.873   3.889   2.354  C Coil           4.630         
ATOM    282 HB3  ASP A  77       9.917   5.837   1.809  C Coil           2.740   2.550 
ATOM    283 N    ASP A  77       9.450   2.725   1.714  C Coil         122.130         
ATOM    284 O    ASP A  77       9.184   4.357  -0.558  C Coil         
ATOM    285 OD1  ASP A  77       8.217   6.602   3.545  C Coil         
ATOM    286 OD2  ASP A  77       9.259   5.160   4.835  C Coil         
ATOM    287 C    ASN A  78       6.070   6.997  -0.815  C Coil         
ATOM    288 CA   ASN A  78       6.779   5.660  -1.003  C Coil          54.780         
ATOM    289 CB   ASN A  78       5.840   4.658  -1.679  C Coil          40.450         
ATOM    290 CG   ASN A  78       6.195   4.427  -3.135  C Coil         178.230         
ATOM    291 H    ASN A  78       6.677   5.234   1.068  C Coil           7.900         
ATOM    292 HA   ASN A  78       7.644   5.811  -1.633  C Coil           4.450         
ATOM    293 HB3  ASN A  78       4.827   5.031  -1.629  C Coil           2.740   2.660 
ATOM    294 HD21 ASN A  78       7.775   3.343  -2.603  C Coil           6.830         
ATOM    295 HD22 ASN A  78       7.528   3.525  -4.303  C Coil           7.500         
ATOM    296 N    ASN A  78       7.244   5.133   0.275  C Coil         123.990         
ATOM    297 ND2  ASN A  78       7.274   3.691  -3.370  C Coil         112.680         
ATOM    298 O'   ASN A  78       5.125   7.056   0.000  C Coil         
ATOM    299 O''  ASN A  78       6.466   7.975  -1.484  C Coil         
ATOM    300 OD1  ASN A  78       5.505   4.902  -4.038  C Coil         
ENDMDL
