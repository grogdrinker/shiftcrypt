ORIGIN        This file is based on BMRB entry bmr17558, PDB code 2lbg.
DATE          Generated on 2012-08-23, original BMRB deposition date 2011-03-31.
MOLNAME       PrP_Conserved_Hydrophobic_Domain
TEMPERATURE   310.0
pH            7.6
IONICSTRENGTH n/a
LABELLING     13C (uniform), 15N (uniform) (original '[U-99% 13C; U-99% 15N]')
EXPTYPE       NMR

    
CORRECTION    C (aliphatic)                   -0.774 +/- 0.112
CORRECTION    C (high ppm, proton attached)   0.036 +/- 0.426 (NOT APPLIED, TOO UNCERTAIN)
CORRECTION    C (high ppm, no proton)         0.405 +/- 0.287 (NOT APPLIED, TOO UNCERTAIN)
CORRECTION    N                               0.275 +/- 0.505 (NOT APPLIED, TOO UNCERTAIN)
CORRECTION    H                               0.049 +/- 0.017 (NOT APPLIED, TOO UNCERTAIN)

MODEL 1
ATOM      1 C    LYS A   1       1.170   0.098  -2.450  H Coil         
ATOM      2 CA   LYS A   1       2.094  -0.002  -1.241  H Coil         
ATOM      3 CB   LYS A   1       2.944  -1.270  -1.338  H Coil         
ATOM      4 CD   LYS A   1       2.826  -3.685  -2.019  H Coil         
ATOM      5 CE   LYS A   1       2.657  -3.555  -3.525  H Coil          41.273         
ATOM      6 CG   LYS A   1       2.132  -2.552  -1.282  H Coil         
ATOM      7 H1   LYS A   1       1.806   0.000   0.856  H Coil         
ATOM      8 HA   LYS A   1       2.746   0.859  -1.229  H Coil           4.639         
ATOM      9 HB2  LYS A   1       3.488  -1.254  -2.271  H Coil         
ATOM     10 HB3  LYS A   1       3.650  -1.280  -0.520  H Coil         
ATOM     11 HD2  LYS A   1       3.879  -3.665  -1.783  H Coil         
ATOM     12 HD3  LYS A   1       2.401  -4.626  -1.698  H Coil         
ATOM     13 HE2  LYS A   1       1.640  -3.807  -3.784  H Coil           2.900         
ATOM     14 HE3  LYS A   1       2.859  -2.533  -3.810  H Coil           2.900         
ATOM     15 HG2  LYS A   1       1.999  -2.838  -0.249  H Coil         
ATOM     16 HG3  LYS A   1       1.167  -2.377  -1.736  H Coil         
ATOM     17 HZ1  LYS A   1       4.397  -3.917  -4.625  H Coil         
ATOM     18 HZ2  LYS A   1       3.088  -4.892  -5.071  H Coil         
ATOM     19 HZ3  LYS A   1       3.929  -5.209  -3.638  H Coil         
ATOM     20 N    LYS A   1       1.329   0.000   0.000  H Coil         
ATOM     21 NZ   LYS A   1       3.583  -4.456  -4.267  H Coil         
ATOM     22 O    LYS A   1       1.293  -0.672  -3.403  H Coil         
ATOM     23 C    HIS A   2      -0.011   1.904  -4.697  H AlphaHelix   177.032         
ATOM     24 CA   HIS A   2      -0.698   1.254  -3.500  H AlphaHelix    58.579         
ATOM     25 CB   HIS A   2      -1.870   2.119  -3.038  H AlphaHelix    29.782         
ATOM     26 CD2  HIS A   2      -4.308   1.485  -2.418  H AlphaHelix   119.910         
ATOM     27 CE1  HIS A   2      -3.872  -0.212  -1.100  H AlphaHelix   138.800         
ATOM     28 CG   HIS A   2      -2.963   1.341  -2.371  H AlphaHelix   
ATOM     29 H    HIS A   2       0.198   1.635  -1.620  H AlphaHelix   
ATOM     30 HA   HIS A   2      -1.072   0.286  -3.798  H AlphaHelix     4.313         
ATOM     31 HB2  HIS A   2      -1.510   2.855  -2.333  H AlphaHelix     3.154         
ATOM     32 HB3  HIS A   2      -2.296   2.624  -3.893  H AlphaHelix     3.154         
ATOM     33 HD1  HIS A   2      -1.840  -0.085  -1.300  H AlphaHelix   
ATOM     34 HD2  HIS A   2      -4.854   2.230  -2.979  H AlphaHelix     6.889         
ATOM     35 HE1  HIS A   2      -3.993  -1.052  -0.432  H AlphaHelix     7.802         
ATOM     36 N    HIS A   2       0.246   1.052  -2.406  H AlphaHelix   
ATOM     37 ND1  HIS A   2      -2.722   0.270  -1.536  H AlphaHelix   
ATOM     38 NE2  HIS A   2      -4.850   0.508  -1.620  H AlphaHelix   
ATOM     39 O    HIS A   2      -0.643   2.168  -5.720  H AlphaHelix   
ATOM     40 C    MET A   3       2.149   1.855  -6.843  H AlphaHelix   177.497         
ATOM     41 CA   MET A   3       2.058   2.780  -5.633  H AlphaHelix    58.587         
ATOM     42 CB   MET A   3       3.462   3.133  -5.140  H AlphaHelix    33.052         
ATOM     43 CE   MET A   3       5.090  -0.371  -5.468  H AlphaHelix    16.374         
ATOM     44 CG   MET A   3       4.146   2.001  -4.390  H AlphaHelix   
ATOM     45 H    MET A   3       1.735   1.927  -3.723  H AlphaHelix     8.055         
ATOM     46 HA   MET A   3       1.550   3.687  -5.925  H AlphaHelix     4.116         
ATOM     47 HB2  MET A   3       4.075   3.394  -5.990  H AlphaHelix     2.300         
ATOM     48 HB3  MET A   3       3.396   3.985  -4.479  H AlphaHelix     2.300         
ATOM     49 HE1  MET A   3       5.416  -0.729  -6.433  H AlphaHelix     1.839         
ATOM     50 HE2  MET A   3       5.547  -0.964  -4.690  H AlphaHelix     1.839         
ATOM     51 HE3  MET A   3       4.015  -0.452  -5.398  H AlphaHelix     1.839         
ATOM     52 HG2  MET A   3       4.475   2.370  -3.429  H AlphaHelix   
ATOM     53 HG3  MET A   3       3.432   1.204  -4.241  H AlphaHelix   
ATOM     54 N    MET A   3       1.286   2.160  -4.562  H AlphaHelix   118.797         
ATOM     55 O    MET A   3       2.209   2.313  -7.983  H AlphaHelix   
ATOM     56 SD   MET A   3       5.572   1.343  -5.274  H AlphaHelix   
ATOM     57 C    ALA A   4       0.935  -0.507  -8.438  H AlphaHelix   178.892         
ATOM     58 CA   ALA A   4       2.241  -0.438  -7.653  H AlphaHelix    55.277         
ATOM     59 CB   ALA A   4       2.589  -1.804  -7.082  H AlphaHelix    18.030         
ATOM     60 H    ALA A   4       2.109   0.247  -5.655  H AlphaHelix     8.279         
ATOM     61 HA   ALA A   4       3.036  -0.141  -8.322  H AlphaHelix     3.965         
ATOM     62 HB1  ALA A   4       2.079  -1.940  -6.139  H AlphaHelix     1.410         
ATOM     63 HB2  ALA A   4       2.279  -2.573  -7.774  H AlphaHelix     1.410         
ATOM     64 HB3  ALA A   4       3.656  -1.868  -6.927  H AlphaHelix     1.410         
ATOM     65 N    ALA A   4       2.159   0.551  -6.585  H AlphaHelix   120.454         
ATOM     66 O    ALA A   4       0.941  -0.667  -9.658  H AlphaHelix   
ATOM     67 C    GLY A   5      -1.777   0.808  -9.186  H AlphaHelix   174.212         
ATOM     68 CA   GLY A   5      -1.481  -0.440  -8.377  H AlphaHelix    47.196         
ATOM     69 H    GLY A   5      -0.127  -0.261  -6.759  H AlphaHelix     8.478         
ATOM     70 HA2  GLY A   5      -1.509  -1.297  -9.033  H AlphaHelix     3.858   3.621 
ATOM     71 HA3  GLY A   5      -2.244  -0.553  -7.620  H AlphaHelix     3.858   3.621 
ATOM     72 N    GLY A   5      -0.183  -0.387  -7.730  H AlphaHelix   104.385         
ATOM     73 O    GLY A   5      -2.223   0.722 -10.330  H AlphaHelix   
ATOM     74 C    ALA A   6      -0.813   3.432 -10.435  H AlphaHelix   179.297         
ATOM     75 CA   ALA A   6      -1.771   3.240  -9.264  H AlphaHelix    55.527         
ATOM     76 CB   ALA A   6      -1.642   4.392  -8.279  H AlphaHelix    18.693         
ATOM     77 H    ALA A   6      -1.174   1.973  -7.678  H AlphaHelix     8.005         
ATOM     78 HA   ALA A   6      -2.784   3.231  -9.639  H AlphaHelix     4.245         
ATOM     79 HB1  ALA A   6      -1.328   4.010  -7.318  H AlphaHelix     1.355         
ATOM     80 HB2  ALA A   6      -0.909   5.097  -8.644  H AlphaHelix     1.355         
ATOM     81 HB3  ALA A   6      -2.597   4.886  -8.176  H AlphaHelix     1.355         
ATOM     82 N    ALA A   6      -1.529   1.970  -8.591  H AlphaHelix   123.200         
ATOM     83 O    ALA A   6      -1.187   3.980 -11.471  H AlphaHelix   
ATOM     84 C    ALA A   7       1.139   2.161 -12.468  H AlphaHelix   178.133         
ATOM     85 CA   ALA A   7       1.436   3.100 -11.304  H AlphaHelix    54.946         
ATOM     86 CB   ALA A   7       2.818   2.817 -10.734  H AlphaHelix    18.011         
ATOM     87 H    ALA A   7       0.662   2.551  -9.412  H AlphaHelix     8.315         
ATOM     88 HA   ALA A   7       1.424   4.119 -11.664  H AlphaHelix     3.863         
ATOM     89 HB1  ALA A   7       2.899   1.766 -10.496  H AlphaHelix     1.400         
ATOM     90 HB2  ALA A   7       3.569   3.080 -11.464  H AlphaHelix     1.400         
ATOM     91 HB3  ALA A   7       2.964   3.401  -9.839  H AlphaHelix     1.400         
ATOM     92 N    ALA A   7       0.424   2.979 -10.261  H AlphaHelix   119.489         
ATOM     93 O    ALA A   7       1.295   2.531 -13.632  H AlphaHelix   
ATOM     94 C    ALA A   8      -0.818   0.385 -13.992  H AlphaHelix   178.857         
ATOM     95 CA   ALA A   8       0.390  -0.046 -13.167  H AlphaHelix    55.315         
ATOM     96 CB   ALA A   8       0.136  -1.402 -12.525  H AlphaHelix    17.863         
ATOM     97 H    ALA A   8       0.605   0.709 -11.202  H AlphaHelix     8.294         
ATOM     98 HA   ALA A   8       1.245  -0.140 -13.822  H AlphaHelix     3.988         
ATOM     99 HB1  ALA A   8      -0.820  -1.387 -12.022  H AlphaHelix     1.605         
ATOM    100 HB2  ALA A   8       0.131  -2.166 -13.287  H AlphaHelix     1.605         
ATOM    101 HB3  ALA A   8       0.916  -1.612 -11.809  H AlphaHelix     1.605         
ATOM    102 N    ALA A   8       0.710   0.945 -12.147  H AlphaHelix   118.612         
ATOM    103 O    ALA A   8      -0.798   0.319 -15.221  H AlphaHelix   
ATOM    104 C    ALA A   9      -2.854   2.577 -14.727  H AlphaHelix   179.548         
ATOM    105 CA   ALA A   9      -3.085   1.269 -13.978  H AlphaHelix    55.116         
ATOM    106 CB   ALA A   9      -4.214   1.429 -12.970  H AlphaHelix    18.212         
ATOM    107 H    ALA A   9      -1.824   0.855 -12.330  H AlphaHelix     8.029         
ATOM    108 HA   ALA A   9      -3.373   0.506 -14.686  H AlphaHelix     3.861         
ATOM    109 HB1  ALA A   9      -5.075   1.858 -13.461  H AlphaHelix     1.376         
ATOM    110 HB2  ALA A   9      -4.474   0.462 -12.565  H AlphaHelix     1.376         
ATOM    111 HB3  ALA A   9      -3.893   2.080 -12.171  H AlphaHelix     1.376         
ATOM    112 N    ALA A   9      -1.869   0.826 -13.308  H AlphaHelix   118.968         
ATOM    113 O    ALA A   9      -3.395   2.786 -15.812  H AlphaHelix   
ATOM    114 C    GLY A  10      -0.836   4.597 -15.963  H AlphaHelix   175.031         
ATOM    115 CA   GLY A  10      -1.758   4.732 -14.767  H AlphaHelix    46.922         
ATOM    116 H    GLY A  10      -1.642   3.235 -13.275  H AlphaHelix     8.386         
ATOM    117 HA2  GLY A  10      -2.686   5.180 -15.090  H AlphaHelix     4.313   3.495 
ATOM    118 HA3  GLY A  10      -1.292   5.380 -14.040  H AlphaHelix     4.313   3.495 
ATOM    119 N    GLY A  10      -2.046   3.455 -14.141  H AlphaHelix   103.412         
ATOM    120 O    GLY A  10      -0.980   5.316 -16.951  H AlphaHelix   
ATOM    121 C    ALA A  11       0.379   2.807 -18.161  H AlphaHelix   179.909         
ATOM    122 CA   ALA A  11       1.063   3.445 -16.956  H AlphaHelix    54.850         
ATOM    123 CB   ALA A  11       2.213   2.571 -16.476  H AlphaHelix    17.384         
ATOM    124 H    ALA A  11       0.178   3.130 -15.060  H AlphaHelix     8.392         
ATOM    125 HA   ALA A  11       1.469   4.402 -17.251  H AlphaHelix     4.159         
ATOM    126 HB1  ALA A  11       1.854   1.566 -16.310  H AlphaHelix     1.282         
ATOM    127 HB2  ALA A  11       2.991   2.558 -17.224  H AlphaHelix     1.282         
ATOM    128 HB3  ALA A  11       2.606   2.970 -15.554  H AlphaHelix     1.282         
ATOM    129 N    ALA A  11       0.114   3.672 -15.873  H AlphaHelix   123.234         
ATOM    130 O    ALA A  11       0.621   3.196 -19.303  H AlphaHelix   
ATOM    131 C    VAL A  12      -2.163   2.074 -19.672  H AlphaHelix   176.877         
ATOM    132 CA   VAL A  12      -1.197   1.133 -18.960  H AlphaHelix    66.909         
ATOM    133 CB   VAL A  12      -1.982  -0.074 -18.413  H AlphaHelix    31.296         
ATOM    134 CG1  VAL A  12      -1.035  -1.206 -18.048  H AlphaHelix    22.107  20.544 
ATOM    135 CG2  VAL A  12      -2.822   0.338 -17.213  H AlphaHelix    22.107  20.544 
ATOM    136 H    VAL A  12      -0.629   1.559 -16.967  H AlphaHelix     7.932         
ATOM    137 HA   VAL A  12      -0.472   0.770 -19.674  H AlphaHelix     3.539         
ATOM    138 HB   VAL A  12      -2.647  -0.426 -19.187  H AlphaHelix     2.147         
ATOM    139 HG11 VAL A  12      -0.268  -0.834 -17.385  H AlphaHelix     0.970   0.792 
ATOM    140 HG12 VAL A  12      -1.587  -1.993 -17.555  H AlphaHelix     0.970   0.792 
ATOM    141 HG13 VAL A  12      -0.577  -1.595 -18.945  H AlphaHelix     0.970   0.792 
ATOM    142 HG21 VAL A  12      -2.981  -0.518 -16.574  H AlphaHelix     0.970   0.792 
ATOM    143 HG22 VAL A  12      -2.307   1.109 -16.661  H AlphaHelix     0.970   0.792 
ATOM    144 HG23 VAL A  12      -3.776   0.715 -17.553  H AlphaHelix     0.970   0.792 
ATOM    145 N    VAL A  12      -0.477   1.825 -17.898  H AlphaHelix   117.423         
ATOM    146 O    VAL A  12      -2.121   2.215 -20.894  H AlphaHelix   
ATOM    147 C    VAL A  13      -3.321   4.815 -20.148  H AlphaHelix   177.998         
ATOM    148 CA   VAL A  13      -4.009   3.645 -19.455  H AlphaHelix    67.178         
ATOM    149 CB   VAL A  13      -4.950   4.190 -18.364  H AlphaHelix    31.442         
ATOM    150 CG1  VAL A  13      -5.983   5.128 -18.969  H AlphaHelix    21.911  20.544 
ATOM    151 CG2  VAL A  13      -5.626   3.045 -17.624  H AlphaHelix    21.911  20.544 
ATOM    152 H    VAL A  13      -3.017   2.561 -17.931  H AlphaHelix     7.679         
ATOM    153 HA   VAL A  13      -4.605   3.109 -20.180  H AlphaHelix     3.499         
ATOM    154 HB   VAL A  13      -4.359   4.750 -17.654  H AlphaHelix     2.003         
ATOM    155 HG11 VAL A  13      -5.709   6.151 -18.754  H AlphaHelix     0.973   0.834 
ATOM    156 HG12 VAL A  13      -6.021   4.981 -20.038  H AlphaHelix     0.973   0.834 
ATOM    157 HG13 VAL A  13      -6.953   4.919 -18.542  H AlphaHelix     0.973   0.834 
ATOM    158 HG21 VAL A  13      -6.674   3.018 -17.882  H AlphaHelix     0.973   0.834 
ATOM    159 HG22 VAL A  13      -5.161   2.112 -17.904  H AlphaHelix     0.973   0.834 
ATOM    160 HG23 VAL A  13      -5.523   3.193 -16.559  H AlphaHelix     0.973   0.834 
ATOM    161 N    VAL A  13      -3.033   2.716 -18.899  H AlphaHelix   117.519         
ATOM    162 O    VAL A  13      -3.755   5.265 -21.208  H AlphaHelix   
ATOM    163 C    GLY A  14      -0.881   6.074 -21.449  H AlphaHelix   175.640         
ATOM    164 CA   GLY A  14      -1.512   6.420 -20.115  H AlphaHelix    46.719         
ATOM    165 H    GLY A  14      -1.944   4.907 -18.698  H AlphaHelix     9.134         
ATOM    166 HA2  GLY A  14      -2.190   7.248 -20.253  H AlphaHelix     3.999   3.611 
ATOM    167 HA3  GLY A  14      -0.734   6.716 -19.427  H AlphaHelix     3.999   3.611 
ATOM    168 N    GLY A  14      -2.244   5.305 -19.541  H AlphaHelix   105.995         
ATOM    169 O    GLY A  14      -0.752   6.930 -22.323  H AlphaHelix   
ATOM    170 C    GLY A  15      -0.857   4.302 -23.991  H AlphaHelix   175.562         
ATOM    171 CA   GLY A  15       0.133   4.380 -22.845  H AlphaHelix    47.102         
ATOM    172 H    GLY A  15      -0.614   4.175 -20.874  H AlphaHelix     8.635         
ATOM    173 HA2  GLY A  15       0.917   5.075 -23.106  H AlphaHelix     4.029   3.465 
ATOM    174 HA3  GLY A  15       0.568   3.402 -22.694  H AlphaHelix     4.029   3.465 
ATOM    175 N    GLY A  15      -0.485   4.814 -21.606  H AlphaHelix   111.347         
ATOM    176 O    GLY A  15      -0.611   4.842 -25.071  H AlphaHelix   
ATOM    177 C    LEU A  16      -3.734   4.802 -25.016  H AlphaHelix   179.090         
ATOM    178 CA   LEU A  16      -3.008   3.481 -24.779  H AlphaHelix    58.136         
ATOM    179 CB   LEU A  16      -4.011   2.402 -24.368  H AlphaHelix    41.769         
ATOM    180 CD1  LEU A  16      -6.135   2.924 -23.145  H AlphaHelix    24.213  23.599 
ATOM    181 CD2  LEU A  16      -4.490   1.319 -22.158  H AlphaHelix    24.213  23.599 
ATOM    182 CG   LEU A  16      -4.662   2.578 -22.996  H AlphaHelix    26.245         
ATOM    183 H    LEU A  16      -2.117   3.222 -22.877  H AlphaHelix     8.137         
ATOM    184 HA   LEU A  16      -2.524   3.181 -25.697  H AlphaHelix     4.096         
ATOM    185 HB2  LEU A  16      -4.797   2.382 -25.107  H AlphaHelix     1.844   1.587 
ATOM    186 HB3  LEU A  16      -3.493   1.453 -24.371  H AlphaHelix     1.844   1.587 
ATOM    187 HD11 LEU A  16      -6.656   2.093 -23.596  H AlphaHelix     0.858   0.815 
ATOM    188 HD12 LEU A  16      -6.239   3.797 -23.772  H AlphaHelix     0.858   0.815 
ATOM    189 HD13 LEU A  16      -6.557   3.128 -22.171  H AlphaHelix     0.858   0.815 
ATOM    190 HD21 LEU A  16      -4.847   1.502 -21.156  H AlphaHelix     0.858   0.815 
ATOM    191 HD22 LEU A  16      -3.444   1.050 -22.125  H AlphaHelix     0.858   0.815 
ATOM    192 HD23 LEU A  16      -5.055   0.512 -22.601  H AlphaHelix     0.858   0.815 
ATOM    193 HG   LEU A  16      -4.179   3.394 -22.477  H AlphaHelix     1.817         
ATOM    194 N    LEU A  16      -1.978   3.629 -23.757  H AlphaHelix   121.900         
ATOM    195 O    LEU A  16      -4.023   5.168 -26.154  H AlphaHelix   
ATOM    196 C    GLY A  17      -3.850   7.858 -24.683  H AlphaHelix   175.280         
ATOM    197 CA   GLY A  17      -4.711   6.788 -24.042  H AlphaHelix    47.885         
ATOM    198 H    GLY A  17      -3.769   5.173 -23.049  H AlphaHelix     8.412         
ATOM    199 HA2  GLY A  17      -5.602   6.653 -24.638  H AlphaHelix     3.730   3.548 
ATOM    200 HA3  GLY A  17      -4.998   7.116 -23.054  H AlphaHelix     3.730   3.548 
ATOM    201 N    GLY A  17      -4.024   5.515 -23.931  H AlphaHelix   105.660         
ATOM    202 O    GLY A  17      -4.331   8.651 -25.491  H AlphaHelix   
ATOM    203 C    GLY A  18      -1.410   8.660 -26.352  H AlphaHelix   175.418         
ATOM    204 CA   GLY A  18      -1.661   8.869 -24.872  H AlphaHelix    47.451         
ATOM    205 H    GLY A  18      -2.243   7.225 -23.670  H AlphaHelix     9.719         
ATOM    206 HA2  GLY A  18      -2.081   9.852 -24.724  H AlphaHelix     4.409   3.788 
ATOM    207 HA3  GLY A  18      -0.720   8.807 -24.347  H AlphaHelix     4.409   3.788 
ATOM    208 N    GLY A  18      -2.571   7.882 -24.319  H AlphaHelix   109.701         
ATOM    209 O    GLY A  18      -1.189   9.619 -27.093  H AlphaHelix   
ATOM    210 C    TYR A  19      -2.387   7.514 -29.051  H AlphaHelix   179.066         
ATOM    211 CA   TYR A  19      -1.211   7.073 -28.186  H AlphaHelix    61.906         
ATOM    212 CB   TYR A  19      -0.983   5.569 -28.345  H AlphaHelix    39.007         
ATOM    213 CD1  TYR A  19      -0.472   6.059 -30.769  H AlphaHelix   
ATOM    214 CD2  TYR A  19      -0.908   3.800 -30.145  H AlphaHelix   132.523         
ATOM    215 CE1  TYR A  19      -0.291   5.666 -32.081  H AlphaHelix   
ATOM    216 CE2  TYR A  19      -0.726   3.398 -31.454  H AlphaHelix   117.902         
ATOM    217 CG   TYR A  19      -0.784   5.135 -29.779  H AlphaHelix   
ATOM    218 CZ   TYR A  19      -0.419   4.335 -32.419  H AlphaHelix   
ATOM    219 H    TYR A  19      -1.622   6.683 -26.147  H AlphaHelix     8.690         
ATOM    220 HA   TYR A  19      -0.324   7.598 -28.509  H AlphaHelix     4.093         
ATOM    221 HB2  TYR A  19      -0.103   5.284 -27.788  H AlphaHelix     3.148   3.000 
ATOM    222 HB3  TYR A  19      -1.838   5.039 -27.953  H AlphaHelix     3.148   3.000 
ATOM    223 HD1  TYR A  19      -0.373   7.100 -30.501  H AlphaHelix   
ATOM    224 HD2  TYR A  19      -1.149   3.068 -29.387  H AlphaHelix     7.084         
ATOM    225 HE1  TYR A  19      -0.050   6.400 -32.836  H AlphaHelix   
ATOM    226 HE2  TYR A  19      -0.827   2.356 -31.719  H AlphaHelix     6.790         
ATOM    227 HH   TYR A  19      -0.978   3.394 -33.999  H AlphaHelix   
ATOM    228 N    TYR A  19      -1.441   7.405 -26.785  H AlphaHelix   123.066         
ATOM    229 O    TYR A  19      -2.231   8.323 -29.965  H AlphaHelix   
ATOM    230 OH   TYR A  19      -0.237   3.940 -33.724  H AlphaHelix   
ATOM    231 C    MET A  20      -4.960   8.828 -29.572  H AlphaHelix   178.549         
ATOM    232 CA   MET A  20      -4.770   7.316 -29.503  H AlphaHelix    58.856         
ATOM    233 CB   MET A  20      -5.997   6.666 -28.860  H AlphaHelix    32.033         
ATOM    234 CE   MET A  20      -6.459   4.078 -27.256  H AlphaHelix    16.659         
ATOM    235 CG   MET A  20      -6.042   6.816 -27.348  H AlphaHelix   
ATOM    236 H    MET A  20      -3.628   6.338 -28.014  H AlphaHelix     8.367         
ATOM    237 HA   MET A  20      -4.655   6.934 -30.506  H AlphaHelix     3.980         
ATOM    238 HB2  MET A  20      -6.887   7.117 -29.272  H AlphaHelix     2.376   2.050 
ATOM    239 HB3  MET A  20      -5.995   5.612 -29.095  H AlphaHelix     2.376   2.050 
ATOM    240 HE1  MET A  20      -7.140   3.706 -28.008  H AlphaHelix     2.073         
ATOM    241 HE2  MET A  20      -5.497   4.272 -27.706  H AlphaHelix     2.073         
ATOM    242 HE3  MET A  20      -6.349   3.342 -26.473  H AlphaHelix     2.073         
ATOM    243 HG2  MET A  20      -5.042   6.704 -26.958  H AlphaHelix   
ATOM    244 HG3  MET A  20      -6.410   7.803 -27.110  H AlphaHelix   
ATOM    245 N    MET A  20      -3.566   6.977 -28.754  H AlphaHelix   119.540         
ATOM    246 O    MET A  20      -4.979   9.413 -30.656  H AlphaHelix   
ATOM    247 SD   MET A  20      -7.109   5.595 -26.560  H AlphaHelix   
ATOM    248 C    LEU A  21      -4.085  11.636 -28.919  H AlphaHelix   178.592         
ATOM    249 CA   LEU A  21      -5.289  10.901 -28.338  H AlphaHelix    58.017         
ATOM    250 CB   LEU A  21      -5.514  11.334 -26.888  H AlphaHelix    42.226         
ATOM    251 CD1  LEU A  21      -6.926  13.312 -27.504  H AlphaHelix    24.492  23.922 
ATOM    252 CD2  LEU A  21      -8.015  11.172 -26.802  H AlphaHelix    24.492  23.922 
ATOM    253 CG   LEU A  21      -6.815  12.088 -26.608  H AlphaHelix    26.626         
ATOM    254 H    LEU A  21      -5.076   8.936 -27.579  H AlphaHelix     8.329         
ATOM    255 HA   LEU A  21      -6.163  11.150 -28.920  H AlphaHelix     4.111         
ATOM    256 HB2  LEU A  21      -5.508  10.447 -26.273  H AlphaHelix     1.881   1.850 
ATOM    257 HB3  LEU A  21      -4.692  11.974 -26.603  H AlphaHelix     1.881   1.850 
ATOM    258 HD11 LEU A  21      -5.993  13.854 -27.489  H AlphaHelix     0.942   0.923 
ATOM    259 HD12 LEU A  21      -7.719  13.951 -27.144  H AlphaHelix     0.942   0.923 
ATOM    260 HD13 LEU A  21      -7.146  13.000 -28.514  H AlphaHelix     0.942   0.923 
ATOM    261 HD21 LEU A  21      -8.728  11.341 -26.009  H AlphaHelix     0.942   0.923 
ATOM    262 HD22 LEU A  21      -7.689  10.143 -26.780  H AlphaHelix     0.942   0.923 
ATOM    263 HD23 LEU A  21      -8.478  11.383 -27.755  H AlphaHelix     0.942   0.923 
ATOM    264 HG   LEU A  21      -6.813  12.426 -25.581  H AlphaHelix     1.560         
ATOM    265 N    LEU A  21      -5.100   9.456 -28.409  H AlphaHelix   119.740         
ATOM    266 O    LEU A  21      -4.203  12.767 -29.388  H AlphaHelix   
ATOM    267 C    GLY A  22      -1.715  11.650 -30.929  H AlphaHelix   174.857         
ATOM    268 CA   GLY A  22      -1.719  11.591 -29.414  H AlphaHelix    46.919         
ATOM    269 H    GLY A  22      -2.892  10.084 -28.500  H AlphaHelix     9.027         
ATOM    270 HA2  GLY A  22      -1.633  12.594 -29.024  H AlphaHelix     3.270         
ATOM    271 HA3  GLY A  22      -0.867  11.013 -29.086  H AlphaHelix     3.270         
ATOM    272 N    GLY A  22      -2.927  10.984 -28.886  H AlphaHelix   106.769         
ATOM    273 O    GLY A  22      -1.514  12.714 -31.514  H AlphaHelix   
ATOM    274 C    SER A  23      -3.233  11.036 -33.583  H AlphaHelix   175.751         
ATOM    275 CA   SER A  23      -1.951  10.430 -33.022  H AlphaHelix    60.105         
ATOM    276 CB   SER A  23      -1.817   8.976 -33.480  H AlphaHelix    63.203         
ATOM    277 H    SER A  23      -2.089   9.690 -31.043  H AlphaHelix     7.893         
ATOM    278 HA   SER A  23      -1.108  10.994 -33.391  H AlphaHelix     3.975         
ATOM    279 HB2  SER A  23      -0.852   8.834 -33.942  H AlphaHelix     3.666   3.422 
ATOM    280 HB3  SER A  23      -1.907   8.322 -32.625  H AlphaHelix     3.666   3.422 
ATOM    281 HG   SER A  23      -3.494   8.102 -33.992  H AlphaHelix   
ATOM    282 N    SER A  23      -1.936  10.505 -31.565  H AlphaHelix   114.334         
ATOM    283 O    SER A  23      -3.252  11.551 -34.701  H AlphaHelix   
ATOM    284 OG   SER A  23      -2.826   8.643 -34.418  H AlphaHelix   
ATOM    285 C    ALA A  24      -5.524  13.032 -33.363  H AlphaHelix   178.717         
ATOM    286 CA   ALA A  24      -5.590  11.516 -33.215  H AlphaHelix    53.839         
ATOM    287 CB   ALA A  24      -6.675  11.127 -32.221  H AlphaHelix    18.749         
ATOM    288 H    ALA A  24      -4.226  10.549 -31.918  H AlphaHelix     7.579         
ATOM    289 HA   ALA A  24      -5.842  11.082 -34.173  H AlphaHelix     4.175         
ATOM    290 HB1  ALA A  24      -6.646  10.060 -32.058  H AlphaHelix     1.480         
ATOM    291 HB2  ALA A  24      -6.506  11.640 -31.286  H AlphaHelix     1.480         
ATOM    292 HB3  ALA A  24      -7.641  11.406 -32.615  H AlphaHelix     1.480         
ATOM    293 N    ALA A  24      -4.304  10.972 -32.799  H AlphaHelix   123.192         
ATOM    294 O    ALA A  24      -6.026  13.593 -34.336  H AlphaHelix   
ATOM    295 C    MET A  25      -3.642  15.570 -33.347  H AlphaHelix   176.479         
ATOM    296 CA   MET A  25      -4.768  15.141 -32.412  H AlphaHelix    55.980         
ATOM    297 CB   MET A  25      -4.507  15.673 -31.002  H AlphaHelix    28.078         
ATOM    298 CE   MET A  25      -5.580  17.376 -27.816  H AlphaHelix    16.765         
ATOM    299 CG   MET A  25      -5.643  16.519 -30.451  H AlphaHelix   
ATOM    300 H    MET A  25      -4.521  13.187 -31.639  H AlphaHelix     7.905         
ATOM    301 HA   MET A  25      -5.698  15.553 -32.776  H AlphaHelix     4.418         
ATOM    302 HB2  MET A  25      -4.355  14.836 -30.337  H AlphaHelix     2.724   2.181 
ATOM    303 HB3  MET A  25      -3.612  16.278 -31.018  H AlphaHelix     2.724   2.181 
ATOM    304 HE1  MET A  25      -6.399  16.676 -27.907  H AlphaHelix     2.169         
ATOM    305 HE2  MET A  25      -4.754  16.900 -27.309  H AlphaHelix     2.169         
ATOM    306 HE3  MET A  25      -5.903  18.236 -27.249  H AlphaHelix     2.169         
ATOM    307 HG2  MET A  25      -6.216  16.912 -31.278  H AlphaHelix   
ATOM    308 HG3  MET A  25      -6.277  15.892 -29.842  H AlphaHelix   
ATOM    309 N    MET A  25      -4.901  13.689 -32.390  H AlphaHelix   115.187         
ATOM    310 O    MET A  25      -3.660  16.674 -33.891  H AlphaHelix   
ATOM    311 SD   MET A  25      -5.059  17.899 -29.448  H AlphaHelix   
ATOM    312 C    SER A  26      -1.936  14.932 -35.864  H AlphaHelix   173.350         
ATOM    313 CA   SER A  26      -1.526  14.979 -34.395  H AlphaHelix    58.569         
ATOM    314 CB   SER A  26      -0.394  13.982 -34.137  H AlphaHelix    61.799         
ATOM    315 H    SER A  26      -2.705  13.826 -33.068  H AlphaHelix     7.710         
ATOM    316 HA   SER A  26      -1.177  15.974 -34.163  H AlphaHelix     4.419         
ATOM    317 HB2  SER A  26       0.227  14.346 -33.334  H AlphaHelix     3.839         
ATOM    318 HB3  SER A  26      -0.817  13.026 -33.862  H AlphaHelix     3.839         
ATOM    319 HG   SER A  26      -0.034  13.218 -35.905  H AlphaHelix   
ATOM    320 N    SER A  26      -2.663  14.690 -33.529  H AlphaHelix   124.236         
ATOM    321 O    SER A  26      -1.396  15.664 -36.694  H AlphaHelix   
ATOM    322 OG   SER A  26       0.408  13.811 -35.293  H AlphaHelix   
ATOM    323 C    ARG A  27      -4.893  13.752 -37.572  C Coil         
ATOM    324 CA   ARG A  27      -3.376  13.922 -37.545  C Coil          57.796         
ATOM    325 CB   ARG A  27      -2.706  12.724 -38.221  C Coil          31.334         
ATOM    326 CD   ARG A  27      -2.051  10.536 -37.172  C Coil          42.813         
ATOM    327 CG   ARG A  27      -3.197  11.381 -37.706  C Coil          28.626         
ATOM    328 CZ   ARG A  27      -0.172   9.235 -38.077  C Coil         
ATOM    329 H    ARG A  27      -3.286  13.511 -35.471  C Coil           7.592         
ATOM    330 HA   ARG A  27      -3.117  14.820 -38.085  C Coil           4.020         
ATOM    331 HB2  ARG A  27      -2.900  12.770 -39.283  C Coil           1.825   1.700 
ATOM    332 HB3  ARG A  27      -1.641  12.782 -38.054  C Coil           1.825   1.700 
ATOM    333 HD2  ARG A  27      -1.354  11.182 -36.660  C Coil           3.150         
ATOM    334 HD3  ARG A  27      -2.449   9.812 -36.477  C Coil           3.150         
ATOM    335 HE   ARG A  27      -1.772   9.803 -39.122  C Coil         
ATOM    336 HG2  ARG A  27      -3.907  11.549 -36.909  C Coil           1.636   1.560 
ATOM    337 HG3  ARG A  27      -3.679  10.851 -38.514  C Coil           1.636   1.560 
ATOM    338 HH11 ARG A  27      -0.005   9.723 -36.124  C Coil         
ATOM    339 HH12 ARG A  27       1.313   8.806 -36.774  C Coil         
ATOM    340 HH21 ARG A  27      -0.043   8.595 -39.990  C Coil         
ATOM    341 HH22 ARG A  27       1.291   8.165 -38.973  C Coil         
ATOM    342 N    ARG A  27      -2.894  14.066 -36.177  C Coil         127.023         
ATOM    343 NE   ARG A  27      -1.347   9.832 -38.240  C Coil         
ATOM    344 NH1  ARG A  27       0.428   9.257 -36.895  C Coil         
ATOM    345 NH2  ARG A  27       0.406   8.614 -39.097  C Coil         
ENDMDL
