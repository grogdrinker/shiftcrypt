#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  shiftcrypt.py
#  
#  Copyright 2018  <@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import warnings
warnings.filterwarnings("ignore")
import parser
import torch
import argparse
from sources.autoenchoder_standalone_version import *
def run_shiftcrypt(args):
	args=args[1:]
	pa = argparse.ArgumentParser()
	
	
	
	pa.add_argument('-i', '--infile',
						help='the input Nmr Exchange Format (NEF) file',
						)
	pa.add_argument('-fit','--fit',
						help='Custom_model_Name. re-runs the training and saves the model as marshalled/Custom_model_Name. For custom model',

						default=None
						)
	pa.add_argument('-o', '--outfile',
						help='output file. Residues with a shiftcrypt value of -10 highlight residues with too many missing values',
						default=None)
	pa.add_argument('-m', '--model',
						help='model to use:\n\t1 --> full model\n\n\t2 --> model that uses just the most common atoms (default) \n\n\t3 --> the method with only N, H  and CA CS. Used for dimers. if you want to load a custom model,use -m NameOfTheModel. Please remember the model is required to be in ./marshalled/ and can be generated with python shiftcrypt.py --fit MOdelName ',
						default='2')
	results = pa.parse_args(args)
	if results.fit:
		from chemical_shifts_custom_model import shifts
		a=autoencoder_transformation(shifts)
		a.fit(training_folder='sources/datasets/rereferenced_nmr/')
		torch.save(a,'marshalled/'+results.fit)
		print '############################################'
		print 'TRAIN SUCCESSFUL! the model has been saved'
		print 'in the "marshalled" folder. To use it run'
		print 'python2 shiftcrypt.py -m ModelName -i input'
		print '############################################'
		print 'The monkeys are listening'
		return
	try:
		prot=parser.parse_official(fil=results.infile)[0]
	except:
		print 'error in the parsing of the file. Please double check the format. If everyhting is correct, please report the bug to @gmail.com'
		return
	try:
		if results.model=='1':
			auto=torch.load('marshalled/autoencoder.mtorch') # the method with the full set of Cs. this may retur a lot of -10 (missing values) because of the scarcity of cs data for some residues
		elif results.model=='2':
			auto=torch.load('marshalled/autoenchoder_only_common_atoms.mtorch') # the method with just the most common Cs values
		elif results.model=='3':
			auto=torch.load('marshalled/autoenchoder_only_N_H_atoms.mtorch') # the method with only N and H CS. Used for dimers
		else:
			try:
				auto=torch.load('marshalled/'+results.model)
			except:
				print "custom model", results.model,'not found'
				return

	except:
		print 'error loading the model. Please double check you installed all the dependencies. If everyhting is correct, please report the bug to @gmail.com'
		return
	print auto.shifts
	asd
	out=auto.transform(prot)
	try:
		print 'running transformation'
		out=auto.transform(prot)
	except:
		print 'error transforming the CS data. Please double check you installed all the dependencies. If everyhting is correct, please report the bug to @gmail.com'
		return
	if results.outfile!=None:
		f=open(results.outfile,'w')
		for i in range(len(prot.seq)):
			f.write(prot.seq[i]+' '+str(out[i])+'\n')
		f.close()
		#print out
	else:
		for i in range(len(prot.seq)):
			print prot.seq[i]+' '+str(out[i])
	
			
def main(args):
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(run_shiftcrypt(sys.argv))
