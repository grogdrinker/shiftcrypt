
## INSTALLATION
ShiftCrypt have been tested on Linux machines only. It may require additional installation steps in order to work on other operating systems. If you need support, please contact gabriele.orlando@vub.be or wim.vranken@vub.be.

Besides the dependencies (see lower), ShiftCrypt can be run directly from the shiftcrypt.py file as:

python shiftcrypt.py [args]

## DEPENDENCIES

the following python packages are required:

- python2.7
- scipy (tested with version 0.19.1)
- numpy (tested with version 1.14.3)
- sklearn (tested with version 0.19.1)
- pytorch (see https://pytorch.org/ for an easy installation guide. Tested with version 0.3.1)

 if you experience a memory error when installing pytorch, please use the --no-cache-dir installation option (for example pip --no-cache-dir  install  http://download.pytorch.org/whl/cpu/torch-0.4.1-cp27-cp27mu-linux_x86_64.whl) 

All these packages are available using pip or anaconda. See also the requirements.txt file.

## USAGE

The tool takes as input NMR Exchange Format (NEF) or NMR-STAR v2.1 files.

To use the tool, run

python shiftcrypt.py [options]

to test the example input, run

python shiftcrypt.py -i input_examples/bmr7339_1.nef

The tool should generate the results in a couple of seconds.
##### Please remember that if you have several missing chemical shift values, you should use the reduced model (option -m 2)

 -h --> show the help
 -i --> input nef file file
 -o --> output file
 \-fit --> the name of a custom model. THIS IS AN ADVANCED OPTION: fits a new model based on the encoding scheme described in chemical_shifts_custom_model.py and saves it in marshalled/ModelName
 -m --> model to use: ShiftCrypt can be used with different encoding schemes:
					

 - -m 1 the model that uses the full set of chemical shifts as described in the paper. It may fail to transform some of the residues due to missing chemical shifts data
 - -m 2 --> the model that uses only H,CA,N,CB,C chemical shifts data (H,CA,N and HA,CA,CB for Gly and Pro respectively). It is a good alternative when dealing with experiments wit ha lot of missing data
 - -m 3 --> the model that uses only N and H atoms (HA,CA,CB for Pro). It has not been used for this work
 - -m ModelName --> loads ModelName as a custom model. ONLY WORKS IF YOU RETRAIN YOUR CUSTOM MODEL
					
It is possible to build a custom encoding scheme changing the self.shifts dictionary in sources/autoenchoder_standalone_version.py --> autoencoder_transformation and re-running the fitting. Remember to run the script from the sources folder. Your model will be saved as marshalled/custom_model.mtorch and you can call it with -m 4. This option can be used to reproduce the training procedure we perform in the paper.

### OUTPUT FORMAT ###

the output is made of two columns: the first one contains the residue name, the second one the ShiftCrypt index. 
If the ShiftCrypt output value is equal to -10, it means that the chemical shifts for that residues were not sufficient to perform the transformation
